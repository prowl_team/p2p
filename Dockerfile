FROM python:3.6

RUN mkdir /etc/p2p
WORKDIR /etc/p2p
ADD . /etc/p2p
# redis
RUN apt-get update && apt-get install redis-server -y

# install pip
RUN pip install -r req.txt


# install motion
RUN apt-get update && apt-get install -y libmicrohttpd-dev python3-dev
RUN apt-get update && apt-get install -y --fix-missing autoconf automake build-essential pkgconf \
                                    libtool git libzip-dev libjpeg-dev \
                                    libavformat-dev libavcodec-dev libavutil-dev libswscale-dev libavdevice-dev \
                                    libwebp-dev \
                                    gdebi-core wget git \
                                    tzdata ffmpeg libsm6 libxext6 libxrender-dev

RUN wget https://github.com/Motion-Project/motion/releases/download/release-4.3.1/buster_motion_4.3.1-1_amd64.deb && gdebi -n buster_motion_4.3.1-1_amd64.deb
RUN pip3 install pip --upgrade
RUN pip3 install setuptools

# timezone
RUN ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && dpkg-reconfigure -f noninteractive tzdata


# Prep Supervisor.
RUN apt-get update && apt-get install -y supervisor && mkdir -p /etc/supervisor && echo_supervisord_conf > /etc/supervisor/supervisord.conf
RUN \
  rm -rf /var/lib/apt/lists/* && \
  sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisor/supervisord.conf && \
  sed -i 's/^\(\[supervisord\]\)$/\1\nuser=root/' /etc/supervisor/supervisord.conf

RUN echo "\n[include]\nfiles = /etc/supervisor/conf.d/*.conf" >> /etc/supervisor/supervisord.conf
RUN echo "\n[inet_http_server]\nport = 0.0.0.0:9001\nusername = admin\npassword = prowl*444" >> /etc/supervisor/supervisord.conf
RUN cp /etc/p2p/supervisor.conf /etc/supervisor/conf.d/supervisor.conf

WORKDIR /etc/p2p

# Define default command.
ENTRYPOINT ["supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]

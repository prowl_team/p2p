from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import serializers
from rest_framework.response import Response
from connection.models import Process
from django.contrib.auth.models import User, Group



class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class ProcessSerializer(serializers.HyperlinkedModelSerializer):

    id      = serializers.ReadOnlyField()
    master_port      = serializers.ReadOnlyField()
    customer_port      = serializers.ReadOnlyField()
    pid      = serializers.ReadOnlyField()
    status      = serializers.ReadOnlyField()


    def create(self, validated_data):
        return Process.objects.create(**validated_data)
    
    class Meta:
        model = Process
        fields = [
            'id',
            'is_active',
            # 'master_address',
            'master_port',
            # 'customer_address',
            'customer_port',
            'pid',
            'detail',
            'status',
            'serial',
            'name',
        ]


class ProcessViewSet(viewsets.ModelViewSet):

    queryset = Process.objects.all()
    serializer_class = ProcessSerializer

    def post(self, request, *args, **kwargs):
        self.create(request, *args, **kwargs)

    # def list(self, request):
    #     queryset = Process.objects.all()
    #     serializer = ProcessSerializer(queryset, many=True)
    #     return Response(serializer.data)

    # def retrieve(self, request, pk=None):
    #     queryset = Process.objects.all()
    #     process = get_object_or_404(queryset, pk=pk)
    #     serializer = self.ProcessSerializer(process)
    #     return Response(serializer.data)

from django.contrib import admin
from .models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = [
        'created_at',
        'name',
        'is_active',
        'master_address',
        'master_port',
        'target_address',
        'target_port',
        'pid',
        'serial',
    ]

    readonly_fields = ['pid', 'serial', 'master_port']

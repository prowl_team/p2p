'''
    Copyright (c) 2018 Retina Servicos de Tecnologia em Monitoramento LTDA.    
    brAIn Data-Pipe [http://www.retinavision.com.br]

    The heartbeat script is responsible for pushing data to the
    cloud4rpi server. There, the team organizes dashboards to
    vizualize the conditions of the cameras and to generate alerts.
    Data sent:
        - Temperature
        - CPU Load
        - Disk space beeing used
        - RAM beeing used 
        - Container status
        - Task queue size
'''
import time
import sys
import cloud4rpi
import psutil
from datetime import datetime, timedelta
import pytz
import glob2
import os
import subprocess
import socket
import re
import json
from requests import get
from owl_client import settings
from owl_client import celery
from owl_client.models import Site, Camera, ResultGroup


def get_localtime():
    return datetime.now(pytz.timezone('America/Sao_Paulo')).strftime('%Y-%m-%d %H:%M:%S')

def get_license_info():
    site=Site.query.all()[0]
    return {'site': site.serial, 'expires_at': '2018-01-01'}

def get_version():
    return settings.VERSION

def get_temperature():
    #temp = check_output(["vcgencmd", "measure_temp"]).decode("UTF-8")
    #return float(re.findall("\d+\.\d+", temp)[0])
    return float(0)

def get_disk_usage():
    return psutil.disk_usage('/').percent

def get_memory_usage():
    return psutil.virtual_memory().percent

def get_cpu_usage():
    return psutil.cpu_percent(interval=None)

def get_processing_delay():
    '''
        To find the process delay (time difference between image aquisition and
        processing time) we get the first image-name that is on the
        /home/pi/data/motion and compare it the current time
    '''
    image_list = sorted(glob2.glob(settings.DIRECTORY_TO_WATCH + '/*.jpg'))
    
    if len(image_list) > 1:
        image_file = image_list[0]

        basename = os.path.basename(image_file)
        local_time = basename.split('-')[1]
        pattern = '%Y%m%d%H%M%S'
        current_epoch = int(time.mktime(time.strptime(local_time, pattern)))

        return round(time.time() - current_epoch)
    
    return 0

def get_motion_dir_size():
    return len(glob2.glob(settings.DIRECTORY_TO_WATCH + '/*'))

def get_docker_status(value=None):
    return True

def get_queue_size(queue_name = "low"):
    # https://gist.github.com/slackorama/3407863
    celery.control.inspect().active()
    return float(0)

def parse_output(pattern, args):
    try:
        out_str = subprocess.check_output(args)
        if isinstance(out_str, bytes):
            out_str = out_str.decode()
    except Exception:
        out_str = ''

    match = re.search(pattern, out_str)
    return match.group(1) if match else None

def get_ip_address():
    response = get('https://api.ipify.org')
    if response.status_code == 200:
        return response.text
    return '127.0.0.1'


def get_host_name():
    return socket.gethostname()


def get_os_name():
    return " ".join(os.uname())

def get_queue_to_send_to_server():
    rgs = ResultGroup.query.filter(
             ResultGroup.send_to_cloud_at==None,
             # ResultGroup.scheduled_to_send_to_cloud_at==None, 
             ResultGroup.created_at >= (datetime.now() - timedelta(days = 1))
         ).all()
     
    return len(rgs)

def get_tasks_data():

    consolidated = dict()

    url = 'http://0.0.0.0:5555/api/tasks'

    response = get(url)
    if response.status_code == 200:
        tasks_data = json.loads(response.content)

        for task in tasks_data:
            task_name = tasks_data[task]['name']
            task_runtime = tasks_data[task]['runtime']
            if tasks_data[task]['succeeded'] is not None:
                task_received_time = datetime.fromtimestamp(tasks_data[task]['succeeded'])

                if task_name in consolidated:
                    if datetime.now() - task_received_time < timedelta(minutes=settings.TASK_RECORD_TIME_MINUTES):
                        consolidated[task_name].append(task_runtime)
                else:
                    consolidated[task_name] = []
        for key in consolidated:
            processing_time_list = consolidated[key]
            consolidated[key] = dict()
            consolidated[key]['count'] = len(processing_time_list)
            if consolidated[key]['count'] > 0:
                consolidated[key]['avg_execution_time'] = sum(processing_time_list)/consolidated[key]['count']
            else:
                consolidated[key]['avg_execution_time'] = 0

    return consolidated


def get_heartbeat_dict():

    site = Site.query.all()[0]

    serial = site.serial

    cameras = Camera.query.all()

    heartbeat = {
        'site_serial': serial,
        'data':{
            'version': get_version(),
            'temperature': get_temperature(),
            'disk_usage_perc': get_disk_usage(),
            'memory_usage_perc': get_memory_usage(),
            'cpu_load_perc': get_cpu_usage(),
            'processing_delay': get_processing_delay(),
            'buffer_directory_motion_size': get_motion_dir_size(),
            'queue_to_send_to_server': get_queue_to_send_to_server(),
            'ip_address': get_ip_address(),
            'host_name': get_host_name(),
            'localtime': get_localtime(),
            'cameras':[],
            'task_status': get_tasks_data(),
            'task_query_time_minutes': settings.TASK_RECORD_TIME_MINUTES
        }
    }

    for camera in cameras:

        heartbeat['data']['cameras'].append({
            'camera_serial':camera.serial,
            'frames_count':camera.get_frames_count(),
            'detections_count':camera.get_detections_count(),
            'avg_threshold_perc_for_detections':camera.get_avg_threshold_perc_for_detections(),
            'avg_processing_time_for_frames':camera.get_avg_processing_time_for_frames(),
            'is_camera_status_active':camera.is_camera_status_active(),
            'is_camera_connection_ok':camera.is_camera_connection_ok()
        })

    return heartbeat


def main():

    # Put variable declarations here
    # Available types: 'bool', 'numeric', 'string'
    variables = {
        'Temperature': {
            'type': 'numeric',
            'bind': get_temperature
        },
        'Disk': {
            'type': 'numeric',
            'bind': get_disk_usage
        },
        'Memory': {
            'type': 'numeric',
            'bind': get_memory_usage
        },
        'CPU Load': {
            'type': 'numeric',
            'bind': get_cpu_usage
        },
        'Delay': {
           'type': 'numeric',
           'bind': get_processing_delay
        },
        'Dir Size': {
            'type': 'numeric',
            'bind': get_motion_dir_size
        },
        'Container Status': {
            'type': 'bool',
            'value': False,
            'bind': get_docker_status
        },
        'Queue Size': {
            'type': 'numeric',
            'bind': get_queue_size
        }
    }

    site = Site.query.all()[0]

    serial = site.serial
    device_token = site.device_token

    diagnostics = {
        'IP Address': get_ip_address,
        'Host': get_host_name,
        'Operating System': get_os_name,
        'Serial': serial
    }
    device = cloud4rpi.connect(device_token)

    try:
        device.declare(variables)
        device.declare_diag(diagnostics)

        device.publish_config()

        # Adds a 1 second delay to ensure device variables are created
        time.sleep(1)

        data_timer = 0
        diag_timer = 0

        while True:
            if data_timer <= 0:

                device.publish_data()
                data_timer = settings.DATA_SENDING_INTERVAL

            if diag_timer <= 0:
                device.publish_diag()
                diag_timer = settings.DIAG_SENDING_INTERVAL

            time.sleep(settings.POLL_INTERVAL)
            diag_timer -= settings.POLL_INTERVAL
            data_timer -= settings.POLL_INTERVAL

    except KeyboardInterrupt:
        cloud4rpi.log.info('Keyboard interrupt received. Stopping...')

    except Exception as e:
        error = cloud4rpi.get_error_message(e)
        cloud4rpi.log.exception("ERROR! %s %s", error, sys.exc_info()[0])

    finally:
        sys.exit(0)


if __name__ == '__main__':
    main()


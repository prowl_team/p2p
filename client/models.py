from django.db import models
import os
import signal
import subprocess
import uuid
import json


class Client(models.Model):

    STATUS_CONNECTION_CHOICES = (
        ('OPEN', 'Aberta'),
        ('CONNECTED', 'Conectado'),
        ('FAILED', 'Falha')
    )

    created_at       = models.DateTimeField(verbose_name='Criado em',auto_now_add=True,)
    is_active        = models.BooleanField(default=True)
    master_address   = models.CharField(max_length=254, default='35.188.255.177')
    master_port      = models.IntegerField(default=10000)
    target_address   = models.CharField(max_length=254, default='0.0.0.0')
    target_port      = models.IntegerField(default=22)
    pid              = models.IntegerField(null=True, blank=True)
    detail           = models.TextField(null=True, blank=True)
    status           = models.CharField(max_length=254, choices=STATUS_CONNECTION_CHOICES, default='OPEN')
    serial           = models.CharField(max_length=254, null=True, blank=True)
    name             = models.CharField(max_length=254, null=True)

    def __str__(self):
        return '{}:{}'.format(self.master_address, self.master_port)

    def get_serial(self):

        cpuserial = str(uuid.uuid4())
        try:
            f = open('/proc/cpuinfo','r')
            for line in f:
                if line[0:6]=='Serial':
                    cpuserial = line[10:26]
            f.close()
        except:
            print('its not a raspberry')

        return cpuserial


    def mount_cmd(self):
        return 'python3 -u client/slaver.py -m {}:{} -t {}:{} &> {}.log'.format(self.master_address, self.master_port, self.target_address, self.target_port,self.name)

    def start(self):
        cmd = self.mount_cmd()
        # cmd = 'ls'
        proc = subprocess.Popen(cmd, shell=True, preexec_fn=os.setsid)
        self.pid = proc.pid
        self.is_active = True
        #self.save()

    def stop(self):
        #if self.is_active:
        try:
            os.killpg(self.pid, signal.SIGTERM)
        except Exception as e:
            print(e)
        self.pid=None
        self.is_active = False

    def pull_master_connection(self):
        import requests

        url = "http://{}:8000/api/processes/".format(self.master_address)

        payload_dict = {'is_active': self.is_active, 'detail': self.detail, 'serial': self.serial, 'name': '{} - {}'.format(self.serial, self.target_port)}

        payload = json.dumps(payload_dict)
        headers = {
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request("POST", url, data=payload, headers=headers)

        return response.status_code, response.content

    def delete(self, *args, **kwargs):
        self.stop()
        super().delete(*args, **kwargs)


    def save(self, *args, **kwargs):
        if self.pk is None:
            self.serial = self.get_serial()
            status_code, content = self.pull_master_connection()
            if status_code == 201:
                self.master_port = json.loads(content).get('master_port')

        if self.is_active:
            self.start()
        else:
            self.stop()
        super().save(*args, **kwargs)


# class Camera(models.Model):
#     created_at       = models.DateTimeField(verbose_name='Criado em',auto_now_add=True,)
#     is_active        = models.BooleanField(default=True)
#     pid              = models.IntegerField(null=True, blank=True)
#     # last_sync_at

#     def conf_to_dict(self, lines):
#         import re

#         data = dict()

#         for line in lines:
#             line = line.strip()
        
#             if len(line) == 0:  # empty line
#                 continue

#             match = re.match('^#\s*(@\w+)\s*(.*)', line)
#             if match:
#                 name, value = match.groups()[:2]

#             elif line.startswith('#') or line.startswith(';'):  # comment line
#                 continue

#             else:
#                 parts = line.split(None, 1)
#                 if len(parts) == 1:  # empty value
#                     parts.append('')

#                 (name, value) = parts

#                 value = value.strip()

#             data[name] = value

#         return data


#     def dict_to_conf(data):
#         conf_lines = []                            
#         for (name, value) in data.items():
#             line = name + ' ' + value
#             conf_lines.append(line)                
#         return conf_lines

#     # def load_configs(self, data):
#     #     with open(src) as f:
#     #         conf = self.dict_to_conf(data)



#     #     return data

#     def parse_motion_picture_file():
#         pass



#     def sync_motion_configs(self):
#         pass


#     def parse_motion_configs(self):
#         pass

#     # def mount_cmd(self):
#     #     return 'motion -u client/slaver.py -m {}:{} -t {}:{} &> {}.log'.format(self.master_address, self.master_port, self.target_address, self.target_port,self.name)
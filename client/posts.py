#-*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import time
import boto3
import json
from owl_client import settings
from owl_client.utils import get_s3_url, get_public_ip, is_brazil_plate_format
from owl_client.models import Site# https://www.peterbe.com/plog/best-practice-with-retries-with-requests


def _requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session 

def send_video_to_s3_bucket(video_src):
    s3 = boto3.client(
        's3', 
          aws_access_key_id = settings.ACCESS_KEY, #auth['S3']['ACCESS_KEY'],
          aws_secret_access_key = settings.SECRET_KEY,# auth['S3']['SECRET_KEY'],
          region_name = settings.REGION #'sa-east-1'
    )
    dst_dir = get_s3_url(video_src, False)
    config = boto3.s3.transfer.TransferConfig(multipart_threshold=1024*25, max_concurrency=10,
                        multipart_chunksize=1024*25, use_threads=True)
    s3.upload_file(
        Filename = video_src, 
        Bucket = settings.S3_BUCKET,
        Key = dst_dir,
        ExtraArgs = { 'ACL': 'public-read', 'ContentType': 'video/mp4'},
        Config = config
    )
    return settings.S3_URL+dst_dir

def send_image_to_s3_bucket(image_src, ):
    """
    TODO:
        - separate event from plate thumbnail in folders
        - separate by cam_name
        - separate by image_quality
        - separate by date
    """
    # pass

    s3 = boto3.client(
        's3', 
          aws_access_key_id = settings.ACCESS_KEY, #auth['S3']['ACCESS_KEY'],
          aws_secret_access_key = settings.SECRET_KEY,# auth['S3']['SECRET_KEY'],
          region_name = settings.REGION #'sa-east-1'
        )

    dst_dir = get_s3_url(image_src, False)

    data = open(image_src, 'rb')

    t0 = time.time()
    try:
        response = s3.put_object(Bucket=settings.S3_BUCKET, Key=dst_dir, Body=data, ACL='public-read')

    except Exception as x:
        response ='It failed :(', x.__class__.__name__

    else:
        # print('It eventually worked', response.status_code, response.text)
        pass

    finally:
        t1 = time.time()
        print (' Took ' + str(t1 - t0) + ' seconds')

    return response


def send_results_to_retina_event_api(results, ):

    site = Site.query.all()[0]

    url = site.url_server_to_post + settings.EVENT_URL
    token = site.token
    data = json.dumps({'results': results})

    return generic_post(url=url, token=token, data=data)


def send_heartbeat_to_retina_api(data):
    site = Site.query.all()[0]

    url = site.url_server_to_post + settings.HEARTBEAT_URL
    token = site.token
    data = json.dumps(data)

    return generic_post(url=url, token=token, data=data)    


def send_record_to_retina_api(data):
    site = Site.query.all()[0]

    url = site.url_server_to_post + settings.RECORD_URL
    token = site.token
    data = json.dumps(data)

    return generic_post(url=url, token=token, data=data)    


def generic_post(url, token, data, timeout=5):
    """
    out: response (object)
    """

    headers = {
        'content-type': "application/json",
        'authorization': "Token {}".format(token),
    }

    t0 = time.time()
    try:
        response = _requests_retry_session().post(
            url, data=data, headers=headers,
            timeout=5
        )
    except Exception as x:
        print('It failed :(', x.__class__.__name__)
        # resp = 'It failed :(' + str(x.__class__.__name__)
        response = None
    else:
        print('It eventually worked', response.status_code, response.text)
        # resp  = 'It eventually worked' + str(response.status_code)+ str(response.text)
    finally:
        t1 = time.time()
        print('Took', t1 - t0, 'seconds')

    return response


def auth(domain='integration.portalretina.com', username='thing', password='prowl*444'):

    url = 'http://{}/api/v1/auth/'.format(domain)

    headers = {
        'Content-Type': "application/json",
    }

    payload = {
        'username':username,
        'password':password
    }

    response = _requests_retry_session().post(url, data=json.dumps(payload), headers=headers)
    return response.status_code, response.content


def get_camera(debug=False):
    site = Site.query.all()[0]
    token = site.token

    url = '{}api/v1/camera'.format(site.url_server_to_post)

    headers = {
        'Accept': 'application/json',
        'Authorization': "Token {}".format(token),
    }

    response = requests.get('{}'.format(url), headers=headers)

    if debug:
        print( response.status_code)
        print (response.content)

    return response.status_code, response.content



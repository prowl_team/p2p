from celery.decorators import periodic_task
from datetime import timedelta, datetime
import time
import os
import json
import base64
from django.conf import settings
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from celery import Celery, shared_task
# from celery.decorators import task

from client.models import Client
# from client.posts import send_video_to_s3_bucket, send_record_to_retina_api
from client.utils import is_file_completed


celery_external = Celery('owl_client',
    broker ='pyamqp://celery:prowl*444@34.82.79.106/owl_client',
    backend='redis://localhost:6379'
)

celery_external.conf.update(
    task_serializer='json',
    accept_content=['json', 'msgpack', 'yaml'],
    result_serializer='json',
    timezone='America/Sao_Paulo',
    enable_utc=True,
)

def _requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session 

def generic_post(url, data, token=None, timeout=40):
    """
    out: response (object)
    """

    headers = {
        'content-type': "application/json",
    }
    if token:
        headers['authorization'] = "Token {}".format(token)

    t0 = time.time()
    try:
        response = _requests_retry_session().post(
            url, data=data, headers=headers,
            timeout=timeout
        )
    except Exception as x:
        print('It failed :(', x.__class__.__name__)
        # resp = 'It failed :(' + str(x.__class__.__name__)
        response = None
    else:
        print('It eventually worked', response.status_code, response.text)
        # resp  = 'It eventually worked' + str(response.status_code)+ str(response.text)
    finally:
        t1 = time.time()
        print('Took', t1 - t0, 'seconds')

    return response

# @periodic_task(run_every=timedelta(minutes=1))
def check_p2p_connection(*args, **kwargs):

    c=Client.objects.filter(is_active=True)

    if not c:
        Client.objects.create()


# @periodic_task(run_every=timedelta(minutes=15))
def restart_active_p2p_connection(*args, **kwargs):

    cs=Client.objects.filter(is_active=True)

    if cs:
        for c in cs:
            c.stop()
            time.sleep(5)
            c.start()


# @periodic_task(run_every=timedelta(minutes=15))
# def pull_heartbeat():
#     pass

# @periodic_task(run_every=timedelta(minutes=15))
# def sync_motion_configurations():
#     pass


# @task(bind=True,  queue='priority')
# @task(bind=True, name="client.tasks.pull_image_to_cloud_to_process")
@shared_task
def pull_image_to_cloud_to_process_using_pyamqp(src):
    if is_file_completed(src):
        with open(src, "rb") as img_file:
            img_string = base64.encodestring(img_file.read()).hex()
        celery_external.send_task(
            "owl_client.tasks.external.execute_alpr_async", 
            queue='external_medium', 
            kwargs={'src':src, 'image_string': img_string,}
        )
        os.remove(src)


@shared_task
def pull_image_to_cloud_to_process_using_flower(src, retries=3):
    if is_file_completed(src):
        with open(src, "rb") as img_file:
            img_string = base64.encodestring(img_file.read()).hex()
        
        task = "owl_client.tasks.external.execute_alpr_async"
        url  = settings.FLOWER_URL + 'api/task/async-apply/' + task
        data = json.dumps({'kwargs': {'src':src, 'image_string': img_string,}})

        for _ in range(retries):
            response = generic_post(url, data)
            if response:
                if response.status_code == 200:
                    os.remove(src)
                    return response.status_code, response.content
            time.sleep(5)

    os.remove(src)
    return (0, None)


# @celery_internal.task(name='owl_client.tasks.internal.send_video_to_s3_bucket_async')
# def pull_video_to_storage(src,):

#     data = get_motion_video_variables(os.path.basename(video_src))
#     camera_id = data['camera_id']# int(os.path.basename(video_src).split('-')[0])

#     camera = Camera.query.get(camera_id)

#     if is_file_completed(src):

#         video_url = send_video_to_s3_bucket(src)

#         data = {
#             'file_url': video_url,
#             'start_at': data['localtime'],
#             'camera_id': camera_id,
#             'serial': camera.serial,
#         }
#         send_record_to_retina_api(data)
#         os.remove(video_src)
#         return True


# def get_motion_video_variables(src):
#     """
#     picture_filename %t-%v-%Y%m%d%H%M%S
                     
#     """
#     try:
#         # Checking if image name path matches template
#         [camera_id,
#         event,
#         localtime,] = src.split('-')
#         localtime = datetime.strptime(localtime.split('.')[0],'%Y%m%d%H%M%S')

#     except:
#         # Image template does not match template - forcing values
#         camera_id = 1
#         localtime = datetime.now()
#         event = 1
#         localtime = time.strftime('%Y-%m-%d %H:%M:%S')

#     return {
#       'camera_id':camera_id,
#       'localtime':localtime,
#       'event':event,
#     }


# @shared_task
# def pull_video_to_cloud(src,):

#     data = get_motion_video_variables(os.path.basename(src))
#     camera_id = data.get('camera_id', 1)

#     if is_file_completed(src):
#         video_url = send_video_to_s3_bucket(src)

#         data = {
#             'file_url': video_url,
#             'start_at': data.get('localtime'),
#             'camera_id': camera_id,
#             'serial': camera.serial,
#         }
#         send_record_to_retina_api(data)
#         os.remove(video_src)
#         return True

# @celery_internal.task(name='owl_client.tasks.internal.send_results_to_retina_event_api_async')
# def send_results_to_retina_event_api_async(results, result_group_id):

#     result_group = ResultGroup.query.get(result_group_id)

#     if result_group.send_to_cloud_at is None:

#         response = send_results_to_retina_event_api(results)

#         if response is not None and response.status_code == 201:
#             result_group = ResultGroup.query.get(result_group_id)
#             result_group.send_to_cloud_at = datetime.now()
#             db.session.add(result_group)
#             db.session.commit()

#             return True

#     return False

import os, time


def is_file_completed(src):
    historicalSize = -1
    while (historicalSize != os.path.getsize(src)):
        historicalSize = os.path.getsize(src)
        time.sleep(2)
    print("[INFO] {} copy has now finished".format(src))
    return True
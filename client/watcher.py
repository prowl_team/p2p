import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from celery import Celery
import glob2
import os
import base64
from datetime import datetime


send_celery = Celery(
    'owl_client',
    broker='pyamqp://celery:prowl*444@34.83.106.70/owl_client',
    backend='redis://0.0.0.0:6379/0'
)

send_celery.conf.update(
    task_serializer='json',
    accept_content=['json', 'msgpack', 'yaml'],
    result_serializer='json',
    timezone='America/Sao_Paulo',
    enable_utc=True,
)


def _get_local_time_min_file(local_time):
    # local_time = os.path.basename(src).split('-')[1]
    local = datetime.strptime(local_time,'%Y%m%d%H%M%S')
    return local.strftime('%Y%m%d%H%M')


def _get_local_time_file(local_time):
    # local_time = os.path.basename(src).split('-')[1]
    local = datetime.strptime(local_time,'%Y%m%d%H%M%S')
    return local.strftime('%Y-%m-%d %H:%M:%S')

    
def _get_motion_variables(src):
    """
    picture_filename %t-%Y%m%d%H%M%S-%v-%q-%D-%N-%i-%J-%K-%L
    """
    try:
      # Checking if image name path matches template
      [camera_id,
      localtime,
      event,
      frame_number, 
      changed_pixels, 
      noise_level, 
      width_motion_area, 
      height_motion_area, 
      x_motion_center, 
      y_motion_center] = src.split('-')

      localtime_min = _get_local_time_min_file(localtime)
      localtime = _get_local_time_file(localtime)

    except:
      # Image template does not match template - forcing values
      camera_id = 1
      localtime = datetime.now()
      event = 1
      frame_number = 1
      changed_pixels = 1
      noise_level = 1
      width_motion_area = 0
      height_motion_area = 0
      x_motion_center = 0
      y_motion_center = 0

      localtime = time.strftime('%Y-%m-%d %H:%M:%S')
      localtime_min = time.strftime('%Y%m%d%H%M')

    return {
      'camera_id':camera_id,
      'localtime':localtime,
      'localtime_min':localtime_min,
      'event':event,
      'frame_number':frame_number,
      'changed_pixels':changed_pixels,
      'noise_level':noise_level,
      'width_motion_area':width_motion_area,
      'height_motion_area':height_motion_area,
      'x_motion_center':x_motion_center,
      'y_motion_center':str(y_motion_center).split('.')[0],
    }


def get_motion_video_variables(src):
    """
    picture_filename %t-%v-%Y%m%d%H%M%S
                     
    """
    try:
      # Checking if image name path matches template
      [camera_id,
      event,
      localtime,] = src.split('-')
      localtime = localtime.split('.')[0]

      localtime_min = _get_local_time_min_file(localtime)
      localtime = _get_local_time_file(localtime)

    except:
      # Image template does not match template - forcing values
      camera_id = 1
      localtime = datetime.now()
      event = 1

      localtime = time.strftime('%Y-%m-%d %H:%M:%S')
      localtime_min = time.strftime('%Y%m%d%H%M')

    return {
      'camera_id':camera_id,
      'localtime':localtime,
      'localtime_min':localtime_min,
      'event':event,
    }


@send_celery.task(name='owl_client.tasks.send_video_to_s3_bucket_async')
def send_video_to_s3_bucket_async(video_src,):

    data = get_motion_video_variables(os.path.basename(video_src))
    camera_id = data['camera_id']# int(os.path.basename(video_src).split('-')[0])

    camera = Camera.query.get(camera_id)

    historicalSize = -1
    while (historicalSize != os.path.getsize(video_src)):
        historicalSize = os.path.getsize(video_src)
        time.sleep(2)
    print(">>>> {} copy has now finished".format(video_src))
    video_url = send_video_to_s3_bucket(video_src)

    data = {
        'file_url': video_url,
        'start_at': data['localtime'],
        'camera_id': camera_id,
        'serial': camera.serial,
    }
    send_record_to_retina_api(data)
    os.remove(video_src)
    return True


class Watcher:

    DIRECTORY_TO_WATCH = './motion/'
    celery = None

    def __init__(self):
        self.observer = Observer()

    def run(self):

        # clean dir
        files = glob2.glob(os.path.join(self.DIRECTORY_TO_WATCH,'*'))
        for f in files:
            os.remove(f)

        event_handler = Handler()
        print('[INFO] Event handler initialized')
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()

        self.observer.join()


class Handler(FileSystemEventHandler):

    def on_created(self, event):
        if os.path.basename(event.src_path) != 'lastsnap.jpg':
            if str(event.src_path).endswith('.jpg'):
                
                print('[INFO] Event is a jpg image\n')

                historicalSize = -1
                while (historicalSize != os.path.getsize(event.src_path)):
                    historicalSize = os.path.getsize(event.src_path)
                    time.sleep(2)
                print("[INFO] {} copy has now finished".format(event.src_path))

                try:
                    with open(event.src_path, "rb") as img_file:
                        print('[INFO] Image is being opened')
                        img_string = base64.encodestring(img_file.read())
                        
                    print('[INFO] img_string len is {}'.format(len(img_string)))
                    send_celery.send_task(
                        "owl_client.tasks.execute_alpr_async", 
                        ignore_result=True, 
                        queue='external_medium', 
                        kwargs={'src':event.src_path, 'image_string': img_string,}
                    )
                    os.remove(event.src_path)
                    print('Done!\n')
                except Exception as e:
                    print('[ERRO] {}'.format(str(e)))

            elif str(event.src_path).endswith('.mp4'):
                send_video_to_s3_bucket_async.apply_async(queue='internal', kwargs={'video_src': event.src_path, })
                print("Received movie obj, sending to cloud")

if __name__ == '__main__':
    w = Watcher()
    w.run()



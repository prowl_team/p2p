from django.contrib import admin
from .models import Process


@admin.register(Process)
class ProcessAdmin(admin.ModelAdmin):
    list_display = [
        'created_at',
        'name',
        'is_active',
        'master_address',
        'master_port',
        'customer_address',
        'customer_port',
        'pid',
    ]

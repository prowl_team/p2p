from django.db import models
import os
import signal
import subprocess


class Process(models.Model):

    STATUS_CONNECTION_CHOICES = (
        ('OPEN', 'Aberta'),
        ('CONNECTED', 'Conectado'),
        ('FAILED', 'Falha')
    )

    created_at       = models.DateTimeField(verbose_name='Criado em',auto_now_add=True,)
    is_active        = models.BooleanField(default=True)
    master_address   = models.CharField(max_length=254, default='0.0.0.0')
    master_port      = models.IntegerField(default=10000)
    customer_address = models.CharField(max_length=254, default='0.0.0.0')
    customer_port    = models.IntegerField(default=10022)
    pid              = models.IntegerField(null=True, blank=True)
    detail           = models.TextField(null=True, blank=True)
    status           = models.CharField(max_length=254, choices=STATUS_CONNECTION_CHOICES, default='OPEN')
    serial           = models.CharField(max_length=254, null=True, blank=True)
    name             = models.CharField(max_length=254, null=True)

    def __str__(self):
        return '{}:{}'.format(self.master_address, self.master_port)

    def mount_cmd(self):
        return 'python3 -u connection/master.py -m {}:{} -c {}:{} &> {}.log'.format(self.master_address, self.master_port, self.customer_address, self.customer_port,self.name)

    def start(self):
        cmd = self.mount_cmd()
        # cmd = 'ls'
        proc = subprocess.Popen(cmd, shell=True, preexec_fn=os.setsid)
        self.pid = proc.pid
        self.is_active = True
        #self.save()

    def stop(self):
        #if self.is_active:
        try:
            os.killpg(self.pid, signal.SIGTERM)
        except Exception as e:
            print(e)
        self.pid=None
        self.is_active = False
        #self.save()

    def save(self, *args, **kwargs):
        if self.pk is None:
            last = Process.objects.last()
            self.master_port = last.master_port + 1
            self.customer_port = last.customer_port + 1
            
        if self.is_active:
            self.start()
        else:
            self.stop()
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.stop()
        super().delete(*args, **kwargs)



class Heartbeat(models.Model):
    pass


class Device(models.Model):
    pass

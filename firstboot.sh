#!/bin/bash
sleep 10
apt-get update
sleep 1m
apt-get install -y python3 python3-pip git
cd /etc && git clone https://gui_ca@bitbucket.org/prowl_team/p2p.git
cd /etc/p2p && pip3 install -r req.txt && python3 manage.py migrate
apt-get install -y supervisor
apt-get install -y redis
cp supervisor.conf /etc/supervisor/conf.d/
service supervisor restart
sed -e '$s/$/\nkernel.panic=3\nkernel.panic_on_oops=3/' -i /etc/sysctl.conf
curl -sSL https://get.docker.com | sh
usermod -aG docker pi
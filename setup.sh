#!/bin/bash

# Update
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
rpi-update

# swap disable
# http://ideaheap.com/2013/07/stopping-sd-card-corruption-on-a-raspberry-pi/
sudo dphys-swapfile swapoff
# sudo dphys-swapfile uninstall
# sudo update-rc.d dphys-swapfile remove

# Kernel panic setup
sed -e '$s/$/\nkernel.panic=3\nkernel.panic_on_oops=3/' -i /etc/sysctl.conf

# crontab
#crontab $(pwd)/crontab.bak

# Remot3
apt-get install -y weavedconnectd

# supervisor
apt-get install -y supervisor

# timezone
ln -fs /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

# install motion
mkdir /etc/p2p/conf.d/
wget https://github.com/Motion-Project/motion/releases/download/release-4.3.1/pi_buster_motion_4.3.1-1_armhf.deb
apt-get install -y gdebi-core
gdebi pi_buster_motion_4.3.1-1_armhf.deb

# install docker
curl -sSL https://get.docker.com | sh
usermod -aG docker $(USER)

# Pull image
#docker login -u retinavision
#docker pull retinavision/brain_arm:develop

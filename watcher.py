import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from celery import Celery
import glob2
import os



celery_internal = Celery('p2p',
    broker ='redis://localhost:6379',
    backend='redis://localhost:6379'
)

celery_internal.conf.update(
    task_serializer='json',
    accept_content=['json', 'msgpack', 'yaml'],
    result_serializer='json',
    timezone='America/Sao_Paulo',
    enable_utc=True,
)


class Watcher:

    DIRECTORY_TO_WATCH = '/etc/p2p/images/motion'

    def __init__(self):
        self.observer = Observer()

    def run(self):

        # clean dir
        files = glob2.glob(os.path.join(self.DIRECTORY_TO_WATCH,'*'))
        for f in files:
            os.remove(f)

        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()

        self.observer.join()



class Handler(FileSystemEventHandler):

    def on_created(self, event):

        if os.path.basename(event.src_path) != 'lastsnap.jpg':
            
            if str(event.src_path).endswith('.jpg'):

                celery_internal.send_task(
                    "client.tasks.pull_image_to_cloud_to_process_using_flower", 
                    # queue='priority', 
                    kwargs={'src': event.src_path, }
                )
                print("Received picture obj")
            
            elif str(event.src_path).endswith('.mp4'):
                celery_internal.send_task(
                    "client.tasks.pull_video_to_storage", 
                    queue='default', 
                    kwargs={'src': event.src_path, }
                )
                print("Received movie obj")

if __name__ == '__main__':
    w = Watcher()
    w.run()
